import serial
from time import sleep
import sys
args = sys.argv
arg_length = len(args)

class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

### connection
arduino_port = "/dev/ttyACM0"
dev = serial.Serial(arduino_port, baudrate=9600, timeout=1)

### methods
def TurnOn():
    print color.BOLD, color.GREEN, "Turning on the FC7", color.END
    dev.write("1\n")
def TurnOff():
    print color.BOLD, color.RED, "Turning off the FC7", color.END
    dev.write("0\n")

### program execution
if arg_length >= 2 and (args[1]=="RST" or args[1]=="ON" or args[1]=="OFF" or args[1]=="1" or args[1]=="0"):

        if args[1] == "OFF" or args[1] == "0":
            TurnOff()
        elif args[1] == "ON" or args[1] == "1":
            TurnOn()
        elif args[1] == "RST":
            TurnOff()
            sleep(3)
            TurnOn()

else:

	print
	print "-> Error! Invalid Arguments!"
	print "-> "
	print "-> Usage:"
	print "-> "
        print "-> fc7_power.py RST|OFF|ON>"
	print "-> "	

dev.close()
