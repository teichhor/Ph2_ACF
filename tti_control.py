import visa
from time import sleep
import sys
args = sys.argv
arg_length = len(args)

hybrid = "8cbc2"

if hybrid == "8cbc2":
	# begin values
	inst1_voltage1 = 5.0
	inst1_current1 = 0.06
	inst1_voltage2 = 5.0
	inst1_current2 = 0.06

	inst2_voltage1 = 1.3
	inst2_current1 = 0.75
	inst2_voltage2 = 3.3
	inst2_current2 = 0.4
	# end values
elif hybrid == "2cbc3":
	# begin values
	inst1_voltage1 = 3.35
	inst1_current1 = 1.0
	inst1_voltage2 = 3.3
	inst1_current2 = 0.25

	inst2_voltage1 = 1.45
	inst2_current1 = 0.4
	inst2_voltage2 = 2.3
	inst2_current2 = 0.1
	# end values
elif hybrid == "mpa":
	# begin values
	inst1_voltage1 = 0.0
	inst1_current1 = 0.1
	inst1_voltage2 = 0.0
	inst1_current2 = 0.1

	inst2_voltage1 = 2.5
	inst2_current1 = 0.5
	inst2_voltage2 = 4.5
	inst2_current2 = 3.0
	# end values

class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

rm = visa.ResourceManager('@py')
inst_1 = rm.open_resource('TCPIP0::192.168.1.180::9221::SOCKET', read_termination='\r\n', write_termination='\r\n')
inst_2 = rm.open_resource('TCPIP0::192.168.1.181::9221::SOCKET', read_termination='\r\n', write_termination='\r\n')

print " Voltage source #1: ", inst_1.query("*IDN?")
print " Voltage source #2: ", inst_2.query("*IDN?")

def SetParameter(which, parameter, value):
	print "Setting ", parameter, " to ", value, " at PS ", which
	if which == 1:
		inst_1.write("%s %f" % (parameter, value))
	elif which == 2:
		inst_2.write("%s %f" % (parameter, value))
	else:
		print "Wrong PS ID"

def GetStatus(which):

	if (which == "BOTH" or which == "1"):
		print color.BOLD + "POWER SUPPLY #1:" + color.END
	
		print "	Settings:"
		print "		Output #1: ", inst_1.query("V1?"), " ", inst_1.query("I1?")
		print "		Output #2: ", inst_1.query("V2?"), " ", inst_1.query("I2?")

		print color.BOLD + "	Applied Voltages:" + color.END
		print color.BOLD + "		Output #1: " + color.END, color.RED + inst_1.query("V1O?"), " ", inst_1.query("I1O?") + color.END
		print color.BOLD + "		Output #2: " + color.END, color.RED + inst_1.query("V2O?"), " ", inst_1.query("I2O?") + color.END

	if (which == "BOTH" or which == "2"):
		print color.BOLD + "POWER SUPPLY #2:" + color.END
	
		print "	Settings:"
		print "		Output #1: ", inst_2.query("V1?"), " ", inst_2.query("I1?")
		print "		Output #2: ", inst_2.query("V2?"), " ", inst_2.query("I2?")

		print color.BOLD + "	Applied Voltages:" + color.END
		print color.BOLD + "		Output #1: " + color.END, color.RED + inst_2.query("V1O?"), " ", inst_2.query("I1O?") + color.END
		print color.BOLD + "		Output #2: " + color.END, color.RED + inst_2.query("V2O?"), " ", inst_2.query("I2O?") + color.END

def TurnOff(which):
	print color.BOLD + color.RED + "TURNING OFF PS: ", which + color.END
	if hybrid == "8cbc2":
		if(which == "1"):
			inst_1.write("OPALL 0")
		elif(which == "2"):
			inst_2.write("OPALL 0")
		elif(which == "BOTH"):
			inst_2.write("OPALL 0")
			sleep(1)
			inst_1.write("OPALL 0")		
	elif hybrid == "2cbc3" and which == "BOTH":
		# in case of 2xcbc3 first the positive voltages has to be turned on, then the others
		# so turning off is reverse order
		inst_2.write("OP2 0") #-2.3
		sleep(0.1)
		inst_1.write("OP2 0") #-3.3V
		sleep(0.5)
		inst_2.write("OP1 0") #1.4V
		sleep(0.1)
		inst_1.write("OP1 0") #3.3V
	elif hybrid == "mpa" and which == "BOTH":		
		inst_2.write("OP1 0") #+2.5V
		sleep(0.5)		
		inst_2.write("OP2 0") #+4.5V

	sleep(1)
	GetStatus(which)
	
def TurnOn(which):
	print color.BOLD + color.RED + "TURNING ON PS: ", which + color.END
	if hybrid == "8cbc2" :
		if(which == "1"):
			inst_1.write("OPALL 1")
		elif(which == "2"):
			inst_2.write("OPALL 1")
		elif(which == "BOTH"):
			inst_1.write("OPALL 1")
			sleep(1)
			inst_2.write("OPALL 1")
	elif hybrid == "2cbc3" and which == "BOTH":
		# in case of 2xcbc3 first the positive voltages has to be turned on, then the others
		inst_1.write("OP1 1") #+3.3V
		sleep(0.5)
		inst_2.write("OP1 1") #+1.4V
		sleep(0.5)
		inst_1.write("OP2 1") #-3.3V
		sleep(0.5)
		inst_2.write("OP2 1") #-2.3V
	elif hybrid == "mpa" and which == "BOTH":		
		inst_2.write("OP2 1") #+4.5V
		sleep(0.5)		
		inst_2.write("OP1 1") #+2.5V
	
	sleep(1)		
	GetStatus(which)

if arg_length >= 3 and (args[2]=="SET" or args[2]=="GET" or args[2]=="OFF" or args[2]=="ON")\
		   and (args[1]=="BOTH" or args[1]=="1" or args[1]=="2"):

	if args[2] == "GET":
		GetStatus(args[1])
	elif args[2] == "OFF":
		TurnOff(args[1])
	elif args[2] == "ON":
		TurnOn(args[1])
	elif args[2] == "SET":
		if (args[1] == "BOTH" or args[1] == "1"):
			SetParameter(1, "V1", inst1_voltage1)
			SetParameter(1, "I1", inst1_current1)
			SetParameter(1, "V2", inst1_voltage2)
			SetParameter(1, "I2", inst1_current2)	
		if (args[1] == "BOTH" or args[1] == "2"):
			SetParameter(2, "V1", inst2_voltage1)
			SetParameter(2, "I1", inst2_current1)
			SetParameter(2, "V2", inst2_voltage2)
			SetParameter(2, "I2", inst2_current2)

		GetStatus(args[1])		

else:

	print
	print "-> Error! Invalid Arguments!"
	print "-> "
	print "-> Usage:"
	print "-> "
	print "-> tti_control.py <BOTH|1|2> <SET|GET|OFF|ON>"
	print "-> "	
